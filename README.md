# About
This is the configuration for adding the LightGBM framework as an external package so it can be used in EventLoop, etc.

The weird name is due to LightGBM naming their shared library `_lightgbm` in their makefile. This is the one we want to link against. And I could not make the ATLAS cmake system understand that the `lightgbm` package would name its library `_lightgbm`, so here we are.

# Setup
You only need to add something to the `CMakeLists.txt` of the package that will be using LightGBM and add a bit to the `CMakeLists.txt` of the WHOLE project.

### Step 0: git clone
Go to your source folder, and do `git clone ssh://git@gitlab.cern.ch:7999/dnielsen/_lightgbm.git`.

### Step 1: Prepare for failure
***Important***: You *might* need to do the following in your `build` folder (so do it now anyway):
```bash
cd build/
#mv x86_64-slc6-gcc62-opt/bin/* x86_64-centos6-gcc62-opt/bin/
#mv x86_64-slc6-gcc62-opt/include/* x86_64-centos6-gcc62-opt/include/
#mv x86_64-slc6-gcc62-opt/lib/lib_lightgbm.so x86_64-centos6-gcc62-opt/lib/
#rm -r x86_64-slc6-gcc62-opt/
ln -s x86_64-centos6-gcc62-opt x86_64-slc6-gcc62-opt
```
If your run script/executable (after compiling) complains about: `YourAnalysisExe: error while loading shared libraries: lib_lightgbm.so: cannot open shared object file: No such file or directory`,  
then you didn't follow this step, and the commented out lines saves you from recompiling.

For some reason, this happens on `centos6` clusters (lxplus is `slc6`) and has been fixed for normal packages but not for these external ones.

### Step 2: In your package
In the `CMakeLists.txt` of your package, add the needed LightGBM environmental variables like in this example:
```cmake
# Build a shared library:
atlas_add_library( YourAnalysis
   YourAnalysis/*.h Root/*.h Root/*.cxx ${_dictionarySource}
   PUBLIC_HEADERS YourAnalysis
   INCLUDE_DIRS ${LIGHTGBM_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} xAODBase xAODRootAccess xAODEventInfo ${LIGHTGBM_LIBRARIES}
   )

# You might need to add Lib after YourAnalysis
# add_dependencies( YourAnalysis _lightgbm )

# executables (unless you use a Python-based run script)
atlas_add_executable(
   YourAnalysisExe util/YourAnalysisExe.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${LIGHTGBM_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} ${LIGHTGBM_LIBRARIES} YourAnalysis
   )
```

If you don't comment out the `add_dependencies` line, `YourAnalysis` is guaranteed to be compiled AFTER LightGBM but the LightGBM repository will be checked with every compilation since it's made to track the git HEAD.

I have commented out the dependency, so we can do `acm compile_pkg YourAnalysis` without recompiling this package every time.

### Step 3: In your project
In the `CMakeLists.txt` in your `source` folder (the folder sitting next to `build` and `run`), add
```cmake
# Include the externals configuration:
include( _lightgbm/externals.cmake )
```
between `find_package( AnalysisBase )` and `# Set up CTest:`.

### Step 4: Compiling for the first time
(If it compiles but fails when running, then you probably did not follow step 1 properly.)

If you commented out `add_dependencies` (like me):  
When you have made your changes in the two `CMakeLists.txt`, you run `acm find_packages && acm compile`. You need to run `acm compile_pkg YourAnalysis` again once more (but only this one time), as LightGBM won't be compiled in time to be linked to your package.

If you didn't comment out `add_dependencies`:  
When you have made your changes in the two `CMakeLists.txt`, you simply run `acm find_packages && acm compile`.

### Note: Stick to a tag and don't check the LightGBM repository constantly
Open the `externals.cmake` file. Below the `GIT_REPOSITORY` line, add `GIT_TAG stable`. Compile. Now you can compile all you want, with or without the `add_dependencies` line.

If you need a specific tag (e.g. v2.2.3), you can see all the tags at https://github.com/Microsoft/LightGBM/tags. Then you'd write `GIT_TAG v2.2.3`.

# Minimal example of usage if you already have the model text file
```cpp
// A bunch of includes
#include <LightGBM/config.h>
#include <LightGBM/dataset_loader.h>
#include <LightGBM/boosting.h>
#include <LightGBM/prediction_early_stop.h>
#include <LightGBM/objective_function.h>
#include <LightGBM/metric.h>
#include <LightGBM/utils/common.h>

// Possible includes, if you're missing them
#include <string>
#include <fstream>
#include <TSystem.h>

// Do the setup
void setup() {
    // Create a gbdt and not from file (since that apparently freezes)
    LightGBM::Boosting* booster = LightGBM::Boosting::CreateBoosting("gbdt", nullptr);

    // Path to your trained model
    std::string filename = gSystem->Getenv("WorkDir_DIR");
    filename += "/data/YourAnalysis/model.txt";

    // Make an input file stream and open your model
    std::ifstream input_file;
    input_file.open( filename, std::ios::in | std::ios::binary );

    // Make an std::string from the file
    std::string model_str((std::istreambuf_iterator<char>(input_file)), std::istreambuf_iterator<char>());

    // Get the length as well since that is needed when passing the raw string
    size_t len = std::strlen(model_str.data());

    // Load the model from the string from your model file
    booster->LoadModelFromString(model_str.data(), len);
}

// Make a prediction
double predict() {
    // Make score variable
    double score;

    // Make an array of the input features in the same order as you trained them!
    const double features[1] = { var1 };

    // If you want to predict on all trees, you still need to make this as nullptr is not supported (anymore?)
    auto early_stopper = CreatePredictionEarlyStopInstance("none", LightGBM::PredictionEarlyStopConfig());

    // Predict on features, save in score
    booster->Predict(features, &score, &early_stopper);

    return score;
}
